"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var HtmlReporter = require('protractor-beautiful-reporter');
var caseLoginData_json_1 = __importDefault(require("./files/testData/caseLoginData.json"));
exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    suites: {
        NavigatetoAllProcessItems: ['test/e2e/tests/ProcessItems.js'],
    },
    // specs: [
    //   'test/specs/Login/caseLogin.js',
    //   'test/e2e/tests/PayFile/caseOpenPayFile.js',
    //   'test/e2e/tests/PayFile/casePayFilePreview.js',
    //   'test/e2e/tests/Schedule/caseOpenScheduleDetail.js',
    //   'test/e2e/tests/Balance/caseOpenBalanceQTDDetail.js',
    //   'test/e2e/tests/Balance/caseOpenBalanceYTDDetail.js',
    //   'test/e2e/tests/Transmission/Auto/caseOpenTransmissionAuto.js',
    //   'test/e2e/tests/Transmission/MeF/caseOpenTransmissionMeF.js',
    //   'test/e2e/tests/CompanyPacket/caseOpenCompanyPacket.js'    
    // ],
    capabilities: {
        browserName: 'chrome'
    },
    onPrepare: function () {
        var globals = require('protractor');
        var browser = globals.browser;
        // doing a browser.get will lead to a transpile error. 
        // undefined does not have a get method
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: './testReports',
            docTitle: 'TaxEx Test Report',
            screenshotsSubfolder: 'images',
            jsonsSubfolder: 'data',
            takeScreenShotsOnlyForFailedSpecs: true
        }).getJasmine2Reporter());
        // browser configurations
        browser.ignoreSynchronization = true;
        browser.waitForAngularEnabled(false);
        browser.get(caseLoginData_json_1.default.website);
        browser.manage().timeouts().implicitlyWait(20000);
        browser.manage().window().maximize();
        // login action
        var btnSignIn = browser.driver.findElement(protractor_1.by.xpath('//html/body/div[3]/fieldset/form/div/div[3]/div/div/span/button/span'));
        protractor_1.element(protractor_1.by.id('EmailAddress')).sendKeys(caseLoginData_json_1.default.credential.email);
        protractor_1.element(protractor_1.by.id('Password')).sendKeys(caseLoginData_json_1.default.credential.password).then(function () {
            btnSignIn.click();
        });
    },
    noGlobals: false,
};
//# sourceMappingURL=conf.js.map