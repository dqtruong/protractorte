import { browser, by, Config, element } from 'protractor';
var HtmlReporter = require('protractor-beautiful-reporter');
import data from "./files/testData/caseLoginData.json";

export let config: Config = {
  framework: 'jasmine',    
  seleniumAddress: 'http://localhost:4444/wd/hub',
  suites: {    
    NavigatetoAllProcessItems: ['test/e2e/tests/ProcessItems.js'],
    // PayFilePreview: ['test/e2e/tests/PayFilePreview.js'],
    // GenerateCompanyPacket: ['test/e2e/tests/StartGenerateCompanyPacket.js'],
    // ForeCastTax: ['test/e2e/tests/StartForeCastTaxes.js'],    
  },
  // specs: [
  //   'test/specs/Login/caseLogin.js',
  //   'test/e2e/tests/PayFile/caseOpenPayFile.js',
  //   'test/e2e/tests/PayFile/casePayFilePreview.js',
  //   'test/e2e/tests/Schedule/caseOpenScheduleDetail.js',
  //   'test/e2e/tests/Balance/caseOpenBalanceQTDDetail.js',
  //   'test/e2e/tests/Balance/caseOpenBalanceYTDDetail.js',
  //   'test/e2e/tests/Transmission/Auto/caseOpenTransmissionAuto.js',
  //   'test/e2e/tests/Transmission/MeF/caseOpenTransmissionMeF.js',
  //   'test/e2e/tests/CompanyPacket/caseOpenCompanyPacket.js'    
  // ],
  capabilities: {
    browserName: 'chrome'
  },
  onPrepare: () => {
    let globals = require('protractor');
    let browser = globals.browser;            
    // doing a browser.get will lead to a transpile error. 
    // undefined does not have a get method
      jasmine.getEnv().addReporter(new HtmlReporter({
        baseDirectory: './testReports'
        ,docTitle: 'TaxEx Test Report'
        ,screenshotsSubfolder: 'images'
        ,jsonsSubfolder: 'data'
        ,takeScreenShotsOnlyForFailedSpecs: true
      }).getJasmine2Reporter());
    // browser configurations
    browser.ignoreSynchronization = true;
    browser.waitForAngularEnabled(false);
    browser.get(data.website);
    browser.manage().timeouts().implicitlyWait(20000);
    browser.manage().window().maximize();
    // login action
    let btnSignIn = browser.driver.findElement(by.xpath('//html/body/div[3]/fieldset/form/div/div[3]/div/div/span/button/span'));
    element(by.id('EmailAddress')).sendKeys(data.credential.email);
    element(by.id('Password')).sendKeys(data.credential.password).then(() =>{    
        btnSignIn.click();    
    });    
  },
  noGlobals: false,
};