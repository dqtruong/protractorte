"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var MainNavMenuCollection = /** @class */ (function () {
    function MainNavMenuCollection() {
        this.GlobalSettings = protractor_1.element(protractor_1.by.css('#MainNavMenu > li:first-child'));
        this.Company = protractor_1.element(protractor_1.by.css('#MainNavMenu > li:nth-child(2)'));
        this.Process = protractor_1.element(protractor_1.by.css('#MainNavMenu > li:nth-child(3)'));
        this.Amendment = protractor_1.element(protractor_1.by.css('#MainNavMenu > li:nth-child(4)'));
        this.History = protractor_1.element(protractor_1.by.css('#MainNavMenu > li:nth-child(5)'));
        this.Tools = protractor_1.element(protractor_1.by.css('#MainNavMenu > li:nth-child(6)'));
        this.About = protractor_1.element(protractor_1.by.css('#MainNavMenu > li:last-child'));
    }
    MainNavMenuCollection.prototype.MouseMoveItem = function (val) {
        protractor_1.browser.driver.actions().mouseMove(val).perform();
    };
    return MainNavMenuCollection;
}());
exports.MainNavMenuCollection = MainNavMenuCollection;
//# sourceMappingURL=MainNavMenu.po.js.map