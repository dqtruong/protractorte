"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var LoginPage = /** @class */ (function () {
    function LoginPage() {
        this.boxEmail = protractor_1.element(protractor_1.by.id('EmailAddress'));
        this.boxPassword = protractor_1.element(protractor_1.by.id('Password'));
        this.buttonSignIn = protractor_1.element(protractor_1.by.xpath('//html/body/div[3]/fieldset/form/div/div[3]/div/div/span/button/span'));
        this.title = "Home";
    }
    LoginPage.prototype.LoginPage = function () { };
    return LoginPage;
}());
exports.LoginPage = LoginPage;
//# sourceMappingURL=PageLogin.po.js.map