import { browser, by, element, $, $$, WebElementPromise, ElementFinder } from 'protractor'

export class MainNavMenuCollection {
    
    public GlobalSettings = element(by.css('#MainNavMenu > li:first-child'));

    public Company = element(by.css('#MainNavMenu > li:nth-child(2)'));
    
    public Process = element(by.css('#MainNavMenu > li:nth-child(3)'));
    
    public Amendment = element(by.css('#MainNavMenu > li:nth-child(4)'));
    
    public History = element(by.css('#MainNavMenu > li:nth-child(5)'));
    
    public Tools = element(by.css('#MainNavMenu > li:nth-child(6)'));
    
    public About = element(by.css('#MainNavMenu > li:last-child'));    

    public MouseMoveItem(val: ElementFinder) {
        browser.driver.actions().mouseMove(val).perform();
    }
}