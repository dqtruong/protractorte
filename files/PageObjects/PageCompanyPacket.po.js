"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var CompanyPacketCollection = /** @class */ (function () {
    function CompanyPacketCollection() {
        //public templateSelectDialog = element(by.id('AvailableCompanyPacketTemplates_SelectedValue'));
        this.templateSelect = protractor_1.element(protractor_1.by.id('AvailableCompanyPacketTemplates_SelectedValue'));
        this.buttonSubmit = protractor_1.element(protractor_1.by.css("#DialogPlaceholder + div")).element(protractor_1.by.css("div.ui-dialog-buttonset button:nth-child(2)"));
    }
    CompanyPacketCollection.prototype.selectTemplate = function (templatename) {
        var _this = this;
        protractor_1.element.all(protractor_1.by.css("#AvailableCompanyPacketTemplates_SelectedValue option"))
            // map all OPTION tags to array object with value is VALUE attribute and text is HTML TEXT of OPTION
            .map(function (elm) {
            return {
                value: elm.getAttribute('value'),
                text: elm.getText()
            };
        })
            // promises are resolved in .THEN, item is an array object that above MAP produces
            // ,then use filter to find the desired OPTION and perform CLICK action
            .then(function (item) {
            // console.log(item);
            item.filter(function (elm) {
                if (elm.text == templatename) {
                    _this.templateSelect.element(protractor_1.by.css("option[value='" + elm.value + "']")).click();
                }
            });
        });
    };
    return CompanyPacketCollection;
}());
exports.CompanyPacketCollection = CompanyPacketCollection;
//# sourceMappingURL=PageCompanyPacket.po.js.map