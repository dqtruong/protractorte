"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var BasicFilterCriteriaCollection = /** @class */ (function () {
    function BasicFilterCriteriaCollection() {
        this.basicFilterNameField = 'BasicFilterCriteria_';
        this.parentDivsField = protractor_1.element(protractor_1.by.id(this.basicFilterNameField + 'DefaultPayCycle'));
        this.quarterNumField = protractor_1.element(protractor_1.by.id(this.basicFilterNameField + 'Quarter'));
        this.startYearField = protractor_1.element(protractor_1.by.css("#" + this.basicFilterNameField + "Year_Start"));
        this.endYearField = protractor_1.element(protractor_1.by.id(this.basicFilterNameField + 'Year_End'));
        this.taxCode = protractor_1.element(protractor_1.by.id(this.basicFilterNameField + 'TaxCode'));
    }
    BasicFilterCriteriaCollection.prototype.BasicFilterCriteriaCollection = function () { };
    return BasicFilterCriteriaCollection;
}());
exports.BasicFilterCriteriaCollection = BasicFilterCriteriaCollection;
//# sourceMappingURL=BasicFilter.po.js.map