"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CompanyTaxTaxesCollection = /** @class */ (function () {
    function CompanyTaxTaxesCollection() {
        this.subCompanyTaxItem = "/html/body/div[1]/ul/li[2]/ul/li[2]/span";
        this.subCompanyTaxTaxesItem = "/html/body/div[1]/ul/li[2]/ul/li[2]/ul/li[1]/a";
        this.actionSelected = "Forecast";
    }
    return CompanyTaxTaxesCollection;
}());
exports.CompanyTaxTaxesCollection = CompanyTaxTaxesCollection;
//# sourceMappingURL=PageCompanyTaxTaxes.po.js.map