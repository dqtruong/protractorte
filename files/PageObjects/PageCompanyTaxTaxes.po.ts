import { element, by, $, $$, ElementFinder } from 'protractor';

export class CompanyTaxTaxesCollection {
    
    public subCompanyTaxItem: string = "/html/body/div[1]/ul/li[2]/ul/li[2]/span";

    public subCompanyTaxTaxesItem: string ="/html/body/div[1]/ul/li[2]/ul/li[2]/ul/li[1]/a";

    public actionSelected: string = "Forecast";
}