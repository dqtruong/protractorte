import { element, by, $ } from 'protractor';

export class ShareElementCollection {
    public buttonExpandBasicFilter = element(by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div[1]/form/div/fieldset/div/fieldset/legend'));

    public buttonApplyFilter = element(by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div[1]/form/div/div[1]/button'));
    
    public buttonPerformAction = element(by.id('PerformActionDialogButton'));

    public selectAction = element(by.id('Action-Select'));
}