import {by, element, $, $$, protractor } from 'protractor'

export class CompanyPacketCollection {

    //public templateSelectDialog = element(by.id('AvailableCompanyPacketTemplates_SelectedValue'));

    private templateSelect = element(by.id('AvailableCompanyPacketTemplates_SelectedValue'));
    
    public buttonSubmit = element(by.css("#DialogPlaceholder + div")).element(by.css("div.ui-dialog-buttonset button:nth-child(2)"));

    public selectTemplate(templatename: string) {
        element.all(by.css("#AvailableCompanyPacketTemplates_SelectedValue option"))
        // map all OPTION tags to array object with value is VALUE attribute and text is HTML TEXT of OPTION
        .map((elm) => {
            return {
                value: elm.getAttribute('value'),
                text: elm.getText() 
            }
        })
        // promises are resolved in .THEN, item is an array object that above MAP produces
        // ,then use filter to find the desired OPTION and perform CLICK action
        .then((item) => {
            // console.log(item);
            item.filter((elm) => {
                if(elm.text == templatename) {
                    this.templateSelect.element(by.css("option[value='"+elm.value+"']")).click();                      
                }
            });  
        });
    }
}