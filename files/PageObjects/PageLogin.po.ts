import { browser, element, by, protractor, $$, $, WebElementPromise } from 'protractor';

export class LoginPage {

    public boxEmail = element(by.id('EmailAddress'));
    
    public boxPassword= element(by.id('Password'));
    
    public buttonSignIn= element(by.xpath('//html/body/div[3]/fieldset/form/div/div[3]/div/div/span/button/span'));
    
    public title: string = "Home"

    public LoginPage() {}
}
