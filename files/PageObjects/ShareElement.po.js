"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var ShareElementCollection = /** @class */ (function () {
    function ShareElementCollection() {
        this.buttonExpandBasicFilter = protractor_1.element(protractor_1.by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div[1]/form/div/fieldset/div/fieldset/legend'));
        this.buttonApplyFilter = protractor_1.element(protractor_1.by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div[1]/form/div/div[1]/button'));
        this.buttonPerformAction = protractor_1.element(protractor_1.by.id('PerformActionDialogButton'));
        this.selectAction = protractor_1.element(protractor_1.by.id('Action-Select'));
    }
    return ShareElementCollection;
}());
exports.ShareElementCollection = ShareElementCollection;
//# sourceMappingURL=ShareElement.po.js.map