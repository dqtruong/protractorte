import {by, browser, element, $, ElementFinder } from 'protractor'

export class BasicFilterCriteriaCollection {
    
    private basicFilterNameField: string = 'BasicFilterCriteria_';

    public parentDivsField: ElementFinder = element(by.id(this.basicFilterNameField+'DefaultPayCycle'));

    public quarterNumField: ElementFinder = element(by.id(this.basicFilterNameField+'Quarter'));

    public startYearField: ElementFinder = element(by.css("#"+this.basicFilterNameField+"Year_Start"));

    public endYearField: ElementFinder = element(by.id(this.basicFilterNameField+'Year_End'));

    public taxCode: ElementFinder = element(by.id(this.basicFilterNameField+'TaxCode'));

    public templateField: string[];

    public BasicFilterCriteriaCollection() {}
}
