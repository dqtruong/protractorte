import { by, browser, element } from 'protractor';
import dataPacket from "../../../files/testData/GenerateCompanyPacket.json";
import { MainNavMenuCollection } from '../../../files/PageObjects/MainNavMenu.po.js';
import { BasicFilterCriteriaCollection } from '../../../files/PageObjects/BasicFilter.po.js';
import { ShareElementCollection } from '../../../files/PageObjects/ShareElement.po.js';
import { CompanyPacketCollection } from '../../../files/PageObjects/PageCompanyPacket.po.js';
// all lines below is used for Company Packet Automation 
// import { MainNavMenuCollection } from './PageObjects/MainNavMenu.po.js';
// import { BasicFilterCriteriaCollection } from './PageObjects/BasicFilter.po.js';
// import { ShareElementCollection } from './PageObjects/ShareElement.po.js';
// import { CompanyPacketCollection } from './PageObjects/PageCompanyPacket.po.js';
// import dataLogin from "../Data/logindata.json";
// import dataPacket from "../Data/packettempdata.json";
describe("Test suite generate Company Packet",() => {
    it('Should navigate to Company Packet page',() => {        
        var appMainNavMenu  = new MainNavMenuCollection();
        appMainNavMenu.MouseMoveItem(appMainNavMenu.Process);
        appMainNavMenu.Process.element(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[11]/a')).click();
        let pageTitle = browser.driver.getTitle();
        expect(pageTitle).toContain('Processing Company Packet');
    });
    it('Should start generating Company Packet', () => {        
        var newShareElements = new ShareElementCollection();
        var newFilter = new BasicFilterCriteriaCollection();
        newShareElements.buttonExpandBasicFilter.click().then(() => {            
            newFilter.parentDivsField.sendKeys(dataPacket.parentDivs);
            newFilter.quarterNumField.sendKeys(dataPacket.quarter);
            newFilter.startYearField.sendKeys(dataPacket.yearFrom);
            newFilter.endYearField.sendKeys(dataPacket.yearTo);
        });        
        newShareElements.buttonApplyFilter.click();
        let selectActionOption = browser.driver.findElement(by.xpath('//select[@id="Action-Select"]/option[text()="Generate Packet"]'));
        selectActionOption.click();
        var newCompanyPacketElements = new CompanyPacketCollection();
        newShareElements.buttonPerformAction.click().then(() => {            
            newCompanyPacketElements.selectTemplate(dataPacket.template);
            // newCompanyPacketElements.buttonSubmit.click();              
        });
        browser.sleep(20000);
        expect(element(by.id('AvailableCompanyPacketTemplates_SelectedValue')).element(by.css("option:checked")).getText()).toContain(dataPacket.template);
    });    
});