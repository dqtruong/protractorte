import { by, browser, element, ElementFinder, protractor } from 'protractor';
import itemProcessData from "../../../files/TestData/caseNavigateAnyPage-test.json";
import { MainNavMenuCollection } from "../../../files/PageObjects/MainNavMenu.po";

describe("Test suite couple menu item in Process menu",() => {
    let menuWaitingTime: number = 200;
    let mainNavMenu = element(by.id("MainNavMenu"));
    let newMenu: MainNavMenuCollection = new MainNavMenuCollection();
    let arrMenu = itemProcessData.items;    
    for(let test = 0; test < arrMenu.length; test++) {        
        let menuItem: ElementFinder = ((value: string) => {
            switch(value) {
                case "Global Settings":
                    return newMenu.GlobalSettings;
                    break;
                case "Company":
                    return newMenu.Company;
                    break;
                case "Process":
                    return newMenu.Process;
                    break;
                case "Amendment":
                    return newMenu.Amendment;
                    break;
                case "History":
                    return newMenu.History;
                    break;
                case "Tools":
                    return newMenu.Tools;
                    break;
                default:
                    return newMenu.About;
            }
        })(arrMenu[test].menu);
        ((singleTest, menu) => {        
            let testSpec = singleTest.specs;
            for(let count = 0; count < testSpec.length; count++) {
                it(testSpec[count].Description,() => {
                    browser.driver.sleep(menuWaitingTime);
                    browser.driver.actions().mouseMove(menu).perform().then(() => {
                        browser.driver.sleep(menuWaitingTime);
                        let subFirstLevelMenuItem = element(by.xpath(testSpec[count].SubFirstLevelItem));
                        let isOkBtnFlag: boolean = (testSpec[count].PopupWindow == undefined) ? false : testSpec[count].PopupWindow;
                        let EC = protractor.ExpectedConditions;
                        let okBtn: ElementFinder = element(by.id("OkBtn"));
                        let isOkBtnClickable = EC.elementToBeClickable(okBtn);
                        if(testSpec[count].SubSecondLevelItem != "none") {                            
                            browser.driver.actions().mouseMove(subFirstLevelMenuItem).perform().then(() =>{
                                browser.driver.sleep(menuWaitingTime);
                                let subSecondLevelMenuItem = element(by.xpath(testSpec[count].SubSecondLevelItem));
                                if(testSpec[count].SubThirdLevelItem) {
                                    browser.driver.actions().mouseMove(subSecondLevelMenuItem).perform().then(() => {
                                        browser.sleep(testSpec[count].WaitingTime);
                                        let subThirdLevelItem = element(by.xpath(testSpec[count].SubThirdLevelItem));
                                        // let titlePage = browser.driver.getTitle();                                        
                                        subThirdLevelItem.click().then(() => {
                                            if (isOkBtnFlag)
                                            {
                                                browser.wait(isOkBtnClickable, 500);
                                                okBtn.click();
                                            } 
                                            browser.sleep(testSpec[count].WaitingTime+200);    
                                            expect(mainNavMenu).toBeDefined();
                                        });                                        
                                    });
                                }
                                else 
                                {
                                    subSecondLevelMenuItem.click().then(() => {
                                        if (isOkBtnFlag)
                                        {
                                            browser.wait(isOkBtnClickable, 500);
                                            okBtn.click();
                                        } 
                                        browser.sleep(testSpec[count].WaitingTime+200);
                                        // let titlePage = browser.driver.getTitle();                                    
                                        expect(mainNavMenu).toBeDefined();
                                    });
                                }                                
                            });
                        }
                        else
                        {
                            subFirstLevelMenuItem.click().then(() => {
                                if (isOkBtnFlag)
                                {
                                    browser.wait(isOkBtnClickable, 500);
                                    okBtn.click();
                                } 
                                browser.sleep(testSpec[count].WaitingTime+200);
                                // let titlePage = browser.driver.getTitle();
                                expect(mainNavMenu).toBeDefined();
                            });
                        }
                    });
                });
            }
        })(arrMenu[test], menuItem);
    }
});