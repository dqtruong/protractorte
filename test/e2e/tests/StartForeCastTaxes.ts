import { element, by, $, $$, browser } from "protractor";
import data from "../../../files/TestData/caseLoginData.json";
import { CompanyTaxTaxesCollection } from "../../../files/PageObjects/PageCompanyTaxTaxes.po";
import { BasicFilterCriteriaCollection } from '../../../files/PageObjects/BasicFilter.po.js';
import { ShareElementCollection } from '../../../files/PageObjects/ShareElement.po.js';
import { MainNavMenuCollection } from '../../../files/PageObjects/MainNavMenu.po.js';
describe("Should open and perform forecasting Taxes",() => {
    let itemsCompanyTaxTaxes: CompanyTaxTaxesCollection = new CompanyTaxTaxesCollection();
    it("Should open Company > Tax > Taxes page",() => {
        browser.sleep(1000);
        let newMenu: MainNavMenuCollection = new MainNavMenuCollection();        
        browser.driver.actions().mouseMove(newMenu.Company).perform().then(() => {            
            browser.sleep(1000);
            browser.driver.actions().mouseMove(element(by.xpath(itemsCompanyTaxTaxes.subCompanyTaxItem))).perform().then(() => {
                element(by.xpath(itemsCompanyTaxTaxes.subCompanyTaxTaxesItem)).click();
                expect(browser.driver.getTitle()).toContain("Company Taxes");
            });
        });                 
    });
    it("Should perform ForeCast action", () => {
        let shareElements: ShareElementCollection = new ShareElementCollection();
        shareElements.buttonExpandBasicFilter.click().then(() =>{
           console.log("Hello"); 
        });
    });
});
