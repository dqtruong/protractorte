"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var casePayFilePreviewData_json_1 = __importDefault(require("../../../files/testData/casePayFilePreviewData.json"));
describe("Test suite for performing Preview a payid", function () {
    // browser.ignoreSynchronization = true
    // browser.waitForAngularEnabled(false)
    // browser.get(data.website)
    // beforeAll(() => {        
    //         let emailInput = browser.driver.findElement(by.id('EmailAddress'))            
    //         let passwordInput = browser.driver.findElement(by.id('Password'))        
    //         // browser.driver.sleep(3000)
    //         let emailValue: string = data.credential.email
    //         let passValue: string = data.credential.password
    //         emailInput.sendKeys(emailValue)
    //         passwordInput.sendKeys(passValue)
    //         // browser.driver.sleep(3000)
    //         let btnSignIn = browser.driver.findElement(by.xpath('//html/body/div[3]/fieldset/form/div/div[3]/div/div/span/button/span'))
    //         btnSignIn.click()                            
    // });
    it('Should navigate to All Pay/File', function () {
        var menuItemProcess = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]'));
        var subMenuItemProcessPayFile = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]'));
        var childSubMenuItemProcessPayFile = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]/ul/li[8]'));
        protractor_1.browser.driver.sleep(1000);
        protractor_1.browser.driver.actions().mouseMove(menuItemProcess).perform();
        protractor_1.browser.driver.sleep(1000);
        protractor_1.browser.driver.actions().mouseMove(subMenuItemProcessPayFile).perform();
        protractor_1.browser.driver.sleep(1000);
        childSubMenuItemProcessPayFile.click();
        var titleHomePage = protractor_1.browser.driver.getTitle();
        expect(titleHomePage).toContain('All Pay/File');
    });
    it('Should perform Preview', function () {
        protractor_1.browser.driver.sleep(3000);
        var expandBasicFilter = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/form/div/fieldset/div/fieldset[1]/legend'));
        var inputPayId = protractor_1.browser.driver.findElement(protractor_1.by.id('BasicFilterCriteria_Id'));
        var btnApply = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/form/div/div[1]/button'));
        expandBasicFilter.click();
        inputPayId.sendKeys(casePayFilePreviewData_json_1.default.filter.payment.id[0]);
        protractor_1.browser.driver.sleep(1000);
        btnApply.click();
        protractor_1.browser.driver.sleep(5000);
        var selectActionOption = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//select[@id="Action-Select"]/option[text()="Preview"]'));
        // let selectAction = browser.driver.findElement(by.id('Action-Select'))
        var btnPerformAction = protractor_1.browser.driver.findElement(protractor_1.by.id('PerformActionDialogButton'));
        // selectAction.click()            
        selectActionOption.click();
        btnPerformAction.click().then(function () {
            protractor_1.browser.driver.sleep(15000);
            var dialogPlaceHolderText = protractor_1.browser.findElement(protractor_1.by.xpath('//div[@id="DialogPlaceholder"]/table[1]/tbody/tr[1]/td[1]/b'));
            expect(dialogPlaceHolderText.getText()).toContain('PayAndFile_Preview - Successful :');
        });
    });
});
//# sourceMappingURL=PayFilePreview.js.map