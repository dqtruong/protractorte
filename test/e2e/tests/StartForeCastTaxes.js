"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var PageCompanyTaxTaxes_po_1 = require("../../../files/PageObjects/PageCompanyTaxTaxes.po");
var ShareElement_po_js_1 = require("../../../files/PageObjects/ShareElement.po.js");
var MainNavMenu_po_js_1 = require("../../../files/PageObjects/MainNavMenu.po.js");
describe("Should open and perform forecasting Taxes", function () {
    var itemsCompanyTaxTaxes = new PageCompanyTaxTaxes_po_1.CompanyTaxTaxesCollection();
    it("Should open Company > Tax > Taxes page", function () {
        protractor_1.browser.sleep(1000);
        var newMenu = new MainNavMenu_po_js_1.MainNavMenuCollection();
        protractor_1.browser.driver.actions().mouseMove(newMenu.Company).perform().then(function () {
            protractor_1.browser.sleep(1000);
            protractor_1.browser.driver.actions().mouseMove(protractor_1.element(protractor_1.by.xpath(itemsCompanyTaxTaxes.subCompanyTaxItem))).perform().then(function () {
                protractor_1.element(protractor_1.by.xpath(itemsCompanyTaxTaxes.subCompanyTaxTaxesItem)).click();
                expect(protractor_1.browser.driver.getTitle()).toContain("Company Taxes");
            });
        });
    });
    it("Should perform ForeCast action", function () {
        var shareElements = new ShareElement_po_js_1.ShareElementCollection();
        shareElements.buttonExpandBasicFilter.click().then(function () {
            console.log("Hello");
        });
    });
});
//# sourceMappingURL=StartForeCastTaxes.js.map