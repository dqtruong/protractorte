import { by, browser } from 'protractor';
import data from "../../../files/testData/caseLoginData.json";
import data1 from "../../../files/testData/casePayFilePreviewData.json";

describe("Test suite for performing Preview a payid",() => {
    // browser.ignoreSynchronization = true
    // browser.waitForAngularEnabled(false)
    // browser.get(data.website)
    // beforeAll(() => {        
    //         let emailInput = browser.driver.findElement(by.id('EmailAddress'))            
    //         let passwordInput = browser.driver.findElement(by.id('Password'))        
    //         // browser.driver.sleep(3000)
    //         let emailValue: string = data.credential.email
    //         let passValue: string = data.credential.password
    //         emailInput.sendKeys(emailValue)
    //         passwordInput.sendKeys(passValue)
    //         // browser.driver.sleep(3000)
    //         let btnSignIn = browser.driver.findElement(by.xpath('//html/body/div[3]/fieldset/form/div/div[3]/div/div/span/button/span'))
    //         btnSignIn.click()                            
    // });
    it('Should navigate to All Pay/File',() => {
        let menuItemProcess = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]'))
        let subMenuItemProcessPayFile = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]'))
        let childSubMenuItemProcessPayFile = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]/ul/li[8]'))
        browser.driver.sleep(1000)
        browser.driver.actions().mouseMove(menuItemProcess).perform()
        browser.driver.sleep(1000)
        browser.driver.actions().mouseMove(subMenuItemProcessPayFile).perform()
        browser.driver.sleep(1000)
        childSubMenuItemProcessPayFile.click()
        let titleHomePage = browser.driver.getTitle()
        expect(titleHomePage).toContain('All Pay/File')
    });
    it('Should perform Preview', () => {    
        browser.driver.sleep(3000)
        let expandBasicFilter = browser.driver.findElement(by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/form/div/fieldset/div/fieldset[1]/legend'))
        let inputPayId = browser.driver.findElement(by.id('BasicFilterCriteria_Id'))
        let btnApply = browser.driver.findElement(by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/form/div/div[1]/button'))                        
        expandBasicFilter.click()
        inputPayId.sendKeys(data1.filter.payment.id[0])
        browser.driver.sleep(1000)
        btnApply.click()
        browser.driver.sleep(5000)
        let selectActionOption = browser.driver.findElement(by.xpath('//select[@id="Action-Select"]/option[text()="Preview"]'))
        // let selectAction = browser.driver.findElement(by.id('Action-Select'))
        let btnPerformAction = browser.driver.findElement(by.id('PerformActionDialogButton'))
        // selectAction.click()            
        selectActionOption.click()
        btnPerformAction.click().then(() => {
            browser.driver.sleep(15000)
            let dialogPlaceHolderText = browser.findElement(by.xpath('//div[@id="DialogPlaceholder"]/table[1]/tbody/tr[1]/td[1]/b'))
            expect(dialogPlaceHolderText.getText()).toContain('PayAndFile_Preview - Successful :')
        })        
    });
});