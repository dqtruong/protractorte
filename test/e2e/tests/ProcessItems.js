"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var caseNavigateAnyPage_test_json_1 = __importDefault(require("../../../files/TestData/caseNavigateAnyPage-test.json"));
var MainNavMenu_po_1 = require("../../../files/PageObjects/MainNavMenu.po");
describe("Test suite couple menu item in Process menu", function () {
    var menuWaitingTime = 200;
    var mainNavMenu = protractor_1.element(protractor_1.by.id("MainNavMenu"));
    var newMenu = new MainNavMenu_po_1.MainNavMenuCollection();
    var arrMenu = caseNavigateAnyPage_test_json_1.default.items;
    for (var test = 0; test < arrMenu.length; test++) {
        var menuItem = (function (value) {
            switch (value) {
                case "Global Settings":
                    return newMenu.GlobalSettings;
                    break;
                case "Company":
                    return newMenu.Company;
                    break;
                case "Process":
                    return newMenu.Process;
                    break;
                case "Amendment":
                    return newMenu.Amendment;
                    break;
                case "History":
                    return newMenu.History;
                    break;
                case "Tools":
                    return newMenu.Tools;
                    break;
                default:
                    return newMenu.About;
            }
        })(arrMenu[test].menu);
        (function (singleTest, menu) {
            var testSpec = singleTest.specs;
            var _loop_1 = function (count) {
                it(testSpec[count].Description, function () {
                    protractor_1.browser.driver.sleep(menuWaitingTime);
                    protractor_1.browser.driver.actions().mouseMove(menu).perform().then(function () {
                        protractor_1.browser.driver.sleep(menuWaitingTime);
                        var subFirstLevelMenuItem = protractor_1.element(protractor_1.by.xpath(testSpec[count].SubFirstLevelItem));
                        var isOkBtnFlag = (testSpec[count].PopupWindow == undefined) ? false : testSpec[count].PopupWindow;
                        var EC = protractor_1.protractor.ExpectedConditions;
                        var okBtn = protractor_1.element(protractor_1.by.id("OkBtn"));
                        var isOkBtnClickable = EC.elementToBeClickable(okBtn);
                        if (testSpec[count].SubSecondLevelItem != "none") {
                            protractor_1.browser.driver.actions().mouseMove(subFirstLevelMenuItem).perform().then(function () {
                                protractor_1.browser.driver.sleep(menuWaitingTime);
                                var subSecondLevelMenuItem = protractor_1.element(protractor_1.by.xpath(testSpec[count].SubSecondLevelItem));
                                if (testSpec[count].SubThirdLevelItem) {
                                    protractor_1.browser.driver.actions().mouseMove(subSecondLevelMenuItem).perform().then(function () {
                                        protractor_1.browser.sleep(testSpec[count].WaitingTime);
                                        var subThirdLevelItem = protractor_1.element(protractor_1.by.xpath(testSpec[count].SubThirdLevelItem));
                                        // let titlePage = browser.driver.getTitle();                                        
                                        subThirdLevelItem.click().then(function () {
                                            if (isOkBtnFlag) {
                                                protractor_1.browser.wait(isOkBtnClickable, 500);
                                                okBtn.click();
                                            }
                                            protractor_1.browser.sleep(testSpec[count].WaitingTime + 200);
                                            expect(mainNavMenu).toBeDefined();
                                        });
                                    });
                                }
                                else {
                                    subSecondLevelMenuItem.click().then(function () {
                                        if (isOkBtnFlag) {
                                            protractor_1.browser.wait(isOkBtnClickable, 500);
                                            okBtn.click();
                                        }
                                        protractor_1.browser.sleep(testSpec[count].WaitingTime + 200);
                                        // let titlePage = browser.driver.getTitle();                                    
                                        expect(mainNavMenu).toBeDefined();
                                    });
                                }
                            });
                        }
                        else {
                            subFirstLevelMenuItem.click().then(function () {
                                if (isOkBtnFlag) {
                                    protractor_1.browser.wait(isOkBtnClickable, 500);
                                    okBtn.click();
                                }
                                protractor_1.browser.sleep(testSpec[count].WaitingTime + 200);
                                // let titlePage = browser.driver.getTitle();
                                expect(mainNavMenu).toBeDefined();
                            });
                        }
                    });
                });
            };
            for (var count = 0; count < testSpec.length; count++) {
                _loop_1(count);
            }
        })(arrMenu[test], menuItem);
    }
});
//# sourceMappingURL=ProcessItems.js.map