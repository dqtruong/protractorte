"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var GenerateCompanyPacket_json_1 = __importDefault(require("../../../files/testData/GenerateCompanyPacket.json"));
var MainNavMenu_po_js_1 = require("../../../files/PageObjects/MainNavMenu.po.js");
var BasicFilter_po_js_1 = require("../../../files/PageObjects/BasicFilter.po.js");
var ShareElement_po_js_1 = require("../../../files/PageObjects/ShareElement.po.js");
var PageCompanyPacket_po_js_1 = require("../../../files/PageObjects/PageCompanyPacket.po.js");
// all lines below is used for Company Packet Automation 
// import { MainNavMenuCollection } from './PageObjects/MainNavMenu.po.js';
// import { BasicFilterCriteriaCollection } from './PageObjects/BasicFilter.po.js';
// import { ShareElementCollection } from './PageObjects/ShareElement.po.js';
// import { CompanyPacketCollection } from './PageObjects/PageCompanyPacket.po.js';
// import dataLogin from "../Data/logindata.json";
// import dataPacket from "../Data/packettempdata.json";
describe("Test suite generate Company Packet", function () {
    it('Should navigate to Company Packet page', function () {
        var appMainNavMenu = new MainNavMenu_po_js_1.MainNavMenuCollection();
        appMainNavMenu.MouseMoveItem(appMainNavMenu.Process);
        appMainNavMenu.Process.element(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]/ul/li[11]/a')).click();
        var pageTitle = protractor_1.browser.driver.getTitle();
        expect(pageTitle).toContain('Processing Company Packet');
    });
    it('Should start generating Company Packet', function () {
        var newShareElements = new ShareElement_po_js_1.ShareElementCollection();
        var newFilter = new BasicFilter_po_js_1.BasicFilterCriteriaCollection();
        newShareElements.buttonExpandBasicFilter.click().then(function () {
            newFilter.parentDivsField.sendKeys(GenerateCompanyPacket_json_1.default.parentDivs);
            newFilter.quarterNumField.sendKeys(GenerateCompanyPacket_json_1.default.quarter);
            newFilter.startYearField.sendKeys(GenerateCompanyPacket_json_1.default.yearFrom);
            newFilter.endYearField.sendKeys(GenerateCompanyPacket_json_1.default.yearTo);
        });
        newShareElements.buttonApplyFilter.click();
        var selectActionOption = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//select[@id="Action-Select"]/option[text()="Generate Packet"]'));
        selectActionOption.click();
        var newCompanyPacketElements = new PageCompanyPacket_po_js_1.CompanyPacketCollection();
        newShareElements.buttonPerformAction.click().then(function () {
            newCompanyPacketElements.selectTemplate(GenerateCompanyPacket_json_1.default.template);
            // newCompanyPacketElements.buttonSubmit.click();              
        });
        protractor_1.browser.sleep(20000);
        expect(protractor_1.element(protractor_1.by.id('AvailableCompanyPacketTemplates_SelectedValue')).element(protractor_1.by.css("option:checked")).getText()).toContain(GenerateCompanyPacket_json_1.default.template);
    });
});
//# sourceMappingURL=StartGenerateCompanyPacket.js.map