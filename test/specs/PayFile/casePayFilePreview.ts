import { by, browser} from 'protractor';
import data from "../../../files/testData/casePayFilePreviewData.json";

it('Should perform Preview', () => {
  browser.driver.sleep(5000)      
  let expandBasicFilter = browser.driver.findElement(by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/form/div/fieldset/div/fieldset[1]/legend'))
  let inputPayId = browser.driver.findElement(by.id('BasicFilterCriteria_Id'))
  let btnApply = browser.driver.findElement(by.xpath('//html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/form/div/div[1]/button'))                        
  expandBasicFilter.click()
  inputPayId.sendKeys(data.filter.payment.id[0])
  browser.driver.sleep(1000)
  btnApply.click()
  browser.driver.sleep(5000)
  let selectActionOption = browser.driver.findElement(by.xpath('//select[@id="Action-Select"]/option[text()="Preview"]'))
  // let selectAction = browser.driver.findElement(by.id('Action-Select'))
  let btnPerformAction = browser.driver.findElement(by.id('PerformActionDialogButton'))
  // selectAction.click()            
  selectActionOption.click()
  btnPerformAction.click()
  browser.driver.sleep(15000)
  let dialogPlaceHolderText = browser.findElement(by.xpath('//div[@id="DialogPlaceholder"]/table[1]/tbody/tr[1]/td[1]/b'))
  expect(dialogPlaceHolderText.getText()).toContain('PayAndFile_Preview - Successful :')
});