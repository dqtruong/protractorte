"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
it('Should navigate to All Pay/File', function () {
    var menuItemProcess = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]'));
    var subMenuItemProcessPayFile = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]'));
    var childSubMenuItemProcessPayFile = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]/ul/li[8]'));
    protractor_1.browser.driver.sleep(1000);
    protractor_1.browser.driver.actions().mouseMove(menuItemProcess).perform();
    protractor_1.browser.driver.sleep(1000);
    protractor_1.browser.driver.actions().mouseMove(subMenuItemProcessPayFile).perform();
    protractor_1.browser.driver.sleep(1000);
    childSubMenuItemProcessPayFile.click();
    var titleHomePage = protractor_1.browser.driver.getTitle();
    expect(titleHomePage).toContain('All Pay/File');
});
//# sourceMappingURL=caseOpenPayFile.js.map