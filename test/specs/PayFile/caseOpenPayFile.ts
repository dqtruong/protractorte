import { by, browser} from 'protractor';
it('Should navigate to All Pay/File',() => {
    let menuItemProcess = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]'))
    let subMenuItemProcessPayFile = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]'))
    let childSubMenuItemProcessPayFile = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[6]/ul/li[8]'))
    browser.driver.sleep(1000)
    browser.driver.actions().mouseMove(menuItemProcess).perform()
    browser.driver.sleep(1000)
    browser.driver.actions().mouseMove(subMenuItemProcessPayFile).perform()
    browser.driver.sleep(1000)
    childSubMenuItemProcessPayFile.click()
    let titleHomePage = browser.driver.getTitle()
    expect(titleHomePage).toContain('All Pay/File')
});
