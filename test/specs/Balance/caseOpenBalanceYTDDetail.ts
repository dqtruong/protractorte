import { by, browser } from 'protractor';
it('Should navigate to Schedule Detail',() => {
    let menuItemProcess = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/span'))
    let subMenuItemProcessPayFile = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[3]/span'))
    let childSubMenuItemProcessPayFile = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[3]/ul/li[4]/a'))
    browser.driver.sleep(1000)
    browser.driver.actions().mouseMove(menuItemProcess).perform()
    browser.driver.sleep(1000)
    browser.driver.actions().mouseMove(subMenuItemProcessPayFile).perform()
    browser.driver.sleep(1000)
    childSubMenuItemProcessPayFile.click()
    browser.driver.sleep(7000)
    let titleHomePage = browser.driver.getTitle()
    expect(titleHomePage).toContain('Balance YTD Detail')
});
