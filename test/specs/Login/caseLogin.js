"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var caseLoginData_json_1 = __importDefault(require("../../../files/testData/caseLoginData.json"));
var PageLogin_po_js_1 = require("../../../files/PageObjects/PageLogin.po.js");
// 2 lines below for running GENERATE PACKETS
// import data from "../Data/logindata.json";
// import { LoginPage } from './PageObjects/PageLogin.po.js';
describe('Test TaxEx', function () {
    protractor_1.browser.ignoreSynchronization = true;
    protractor_1.browser.waitForAngularEnabled(false);
    protractor_1.browser.get(caseLoginData_json_1.default.website);
    it('Should be able to login', function () {
        var newapp = new PageLogin_po_js_1.LoginPage();
        protractor_1.browser.driver.sleep(1000).then(function () {
            newapp.boxEmail.sendKeys(caseLoginData_json_1.default.credential.email);
            newapp.boxPassword.sendKeys(caseLoginData_json_1.default.credential.password);
        });
        newapp.buttonSignIn.click();
        protractor_1.browser.sleep(2000);
        expect(newapp.title).toContain('Home');
    });
});
//# sourceMappingURL=caseLogin.js.map