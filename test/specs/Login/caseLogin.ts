import { by, browser} from 'protractor';
import data from "../../../files/testData/caseLoginData.json";
import { LoginPage } from '../../../files/PageObjects/PageLogin.po.js';
// 2 lines below for running GENERATE PACKETS
// import data from "../Data/logindata.json";
// import { LoginPage } from './PageObjects/PageLogin.po.js';
describe('Test TaxEx', function() {
    browser.ignoreSynchronization = true;
    browser.waitForAngularEnabled(false);
    browser.get(data.website);
    it('Should be able to login', function() {
      let newapp = new LoginPage();      
      browser.driver.sleep(1000).then(() => {          
          newapp.boxEmail.sendKeys(data.credential.email);
          newapp.boxPassword.sendKeys(data.credential.password);
      });                  
      newapp.buttonSignIn.click(); 
      browser.sleep(2000);
      expect(newapp.title).toContain('Home');
    });
});