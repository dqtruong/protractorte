import { by, browser } from 'protractor';
it('Should navigate to Company Packet page',() => {
    let menuItemProcess = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]'))
    let menuItemProcessCompanyPacket = browser.driver.findElement(by.xpath('//html/body/div[1]/ul/li[3]/ul/li[11]/a'))        
    browser.driver.sleep(1000)
    browser.driver.actions().mouseMove(menuItemProcess).perform()
    browser.driver.sleep(1000)
    menuItemProcessCompanyPacket.click()
    let titleHomePage = browser.driver.getTitle()
    expect(titleHomePage).toContain('Processing Company Packet')
});
