"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
it('Should navigate to Company Packet page', function () {
    var menuItemProcess = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]'));
    var menuItemProcessCompanyPacket = protractor_1.browser.driver.findElement(protractor_1.by.xpath('//html/body/div[1]/ul/li[3]/ul/li[11]/a'));
    protractor_1.browser.driver.sleep(1000);
    protractor_1.browser.driver.actions().mouseMove(menuItemProcess).perform();
    protractor_1.browser.driver.sleep(1000);
    menuItemProcessCompanyPacket.click();
    var titleHomePage = protractor_1.browser.driver.getTitle();
    expect(titleHomePage).toContain('Processing Company Packet');
});
//# sourceMappingURL=caseOpenCompanyPacket.js.map